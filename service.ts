export module Services
{
	var instances:{[serviceName:string]:Object} = {};

	var classes:{[serviceName:string]:any} = {};


	export function get<T>(serviceName:string, fallbackClass?:{new():T;}):T {
		if (instances[serviceName]) {
			return <T>instances[serviceName];
		}

		if (classes[serviceName]) {
			return instances[serviceName] = new classes[serviceName]();
		}

		if (fallbackClass) {
			return instances[serviceName] = new fallbackClass();
		}

		return null;
	}


	export function register(serviceName:string, serviceClass:any) {
		var instance = instances[serviceName];
		if (instance) {
			if (!(instance instanceof serviceClass)) {
				throw new Error('Service `' + serviceName + '` already initialized.');
			}
		} else {
			classes[serviceName] = serviceClass;
		}
	}
}


export function Service(serviceName:string)
{
	return function(servicePrototype:any):any {
		Services.register(serviceName, servicePrototype);
		return servicePrototype;
	}
}
