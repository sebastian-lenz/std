import {View} from './views/view';


export interface IComponentOptions
{
	allowChildComponents?:boolean;
}


/**
 * A class annotation that registers view classes as components.
 *
 * @param selector
 * @returns {function(T): T}
 * @constructor
 */
export function Component(selector:string, options?:IComponentOptions)
{
	options = _.defaults(options || {}, {
		allowChildComponents: false
	});

	return function<T extends typeof View>(viewClass:T):T {
		Components.register(viewClass, selector, options);
		return viewClass;
	}
}


/**
 * The component registry stores data of all known components.
 */
export module Components
{
	var components:IComponent[] = [];


	export function create(root:HTMLElement = document.body) {
		var tree = new CreationTree(root);

		for (var i = 0, c = components.length; i < c; i++) {
			tree.discover(components[i]);
		}

		return tree.create();
	}


	export function register(viewClass:typeof View, selector:string, options:IComponentOptions) {
		components.push({viewClass, selector, options});
	}
}


/**
 * Definition of the data stored for each component by the registry.
 */
interface IComponent
{
	viewClass:typeof View;

	selector:string;

	options:IComponentOptions
}


class CreationTree
{
	protected _element:HTMLElement;

	protected _children:CreationTree[];

	protected _component:IComponent;

	protected _instance:View;

	protected _isCreated:boolean;


	constructor(element:HTMLElement) {
		this._element = element;
	}


	discover(component:IComponent) {
		var elements = this._element.querySelectorAll(component.selector);

		for (var i = 0, c = elements.length; i < c; i++) {
			var node = this.addElement(<HTMLElement>elements[i]);
			if (node) {
				node.setComponent(component);
			}
		}
	}


	getComponents(result:View[] = []):View[] {
		if (this._instance) {
			result.push(this._instance);
		}

		return this.getChildComponents(result);
	}


	getChildComponents(result:View[] = []):View[] {
		if (this._children) {
			for (var child of this._children) {
				child.getComponents(result);
			}
		}

		return result;
	}


	create(result:View[] = []):View[] {
		if (this._isCreated) return result;
		this._isCreated = true;

		if (this._component) {
			this.createLocalComponents(result);
		} else {
			this.createChildComponents(result);
		}

		return result;
	}


	protected createLocalComponents(result:View[] = []):View[] {
		var component = this._component;
		var viewClass = <any>component.viewClass;
		var viewOptions = <any>{el: this._element};

		if (component.options.allowChildComponents) {
			this.createChildComponents(result);
			viewOptions.components = this.getChildComponents();
		}

		result.push(this._instance = new viewClass(viewOptions));
		return result;
	}


	protected createChildComponents(result:View[] = []):View[] {
		if (this._children) {
			for (var i = 0, c = this._children.length; i < c; ++i) {
				this._children[i].create(result);
			}
		}

		return result;
	}


	protected setComponent(component:IComponent) {
		if (this._component) {
			console.log('Warn: Duplicate component assignment for `' +
				this._component.selector + '` and `' + component.selector + '`.');
			return;
		}

		this._component = component;
	}


	protected addElement(element:HTMLElement):CreationTree {
		var path = [];

		while (element) {
			if (element == this._element) {
				return this.addChildren(path);
			} else {
				path.unshift(element);
				element = <HTMLElement>element.parentNode;
			}
		}

		return null;
	}


	protected addChild(element:HTMLElement):CreationTree {
		if (element.parentNode != this._element) {
			return null;
		}

		if (this._children) {
			for (var i = 0, c = this._children.length; i < c; ++i) {
				if (this._children[i]._element == element) {
					return this._children[i];
				}
			}
		} else {
			this._children = [];
		}

		var child = new CreationTree(element);
		this._children.push(child);
		return child;
	}


	protected addChildren(elements:HTMLElement[]):CreationTree {
		var child;
		if (elements.length) {
			child = this.addChild(elements.shift());
		}

		if (elements.length && child) {
			return child.addChildren(elements);
		} else {
			return child;
		}
	}
}
