export declare class Events extends Backbone.Events {}
declare var exports;

(function(exports:any) {
	function Events() {};
	_.extend(Events.prototype, Backbone.Events);
	exports.Events = Events;
})(exports);
