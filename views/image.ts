import {Visibility, IVisibilityTarget} from '../services/visibility';
import {View, ViewOptions, Option} from "./view";
import {trim} from '../utils/strings';


export enum ImageSourceMode
{
	Width,

	Density
}


export interface IImageSource
{
	src:string;

	bias?:number;

	mode?:ImageSourceMode;
}


export class ImageSourceSet
{
	sources:IImageSource[] = [];

	mode:ImageSourceMode;

	static SOURCE = /([^ ]+) (\d+)([WwXx])/;


	constructor(sources:IImageSource[]);
	constructor(mode:ImageSourceMode);
	constructor(srcSet:string);
	constructor(arg?:any) {
		if (_(arg).isArray()) {
			_(arg).each((source) => this.add(source));
		} else if (arg in ImageSourceMode) {
			this.mode = <ImageSourceMode>arg;
		} else if (typeof(arg) == 'string') {
			this.parse(arg);
		}
	}


	parse(str:string) {
		var sources = str.split(/,/);
		for (var source of sources) {
			source = trim(source);

			var match = ImageSourceSet.SOURCE.exec(source);
			if (match) {
				var mode = (match[3] === 'W' || match[3] === 'w') ? ImageSourceMode.Width : ImageSourceMode.Density;
				this.add({src:match[1], bias:parseFloat(match[2]), mode});
			} else {
				this.add({src:source});
			}
		}
	}


	add(source:IImageSource) {
		if (source.mode != void 0) {
			if (!this.mode) {
				this.mode = source.mode;
			} else if (this.mode != source.mode) {
				console.log('Mismatched image stcSet, all sources must use same mode.');
				return;
			}
		}

		if (source.bias === void 0) {
			source.bias = Number.MAX_VALUE;
		}

		this.sources.push(source);
		this.sources.sort((a, b) => a.bias - b.bias);
	}


	get(el:HTMLElement):string {
		var count = this.sources.length;
		if (count === 0) {
			return '';
		}

		var threshold = window.devicePixelRatio ? window.devicePixelRatio : 1;
		if (this.mode === ImageSourceMode.Width) {
			threshold = el.offsetWidth * threshold;
		}

		for (var source of this.sources) {
			if (source.bias >= threshold) return source.src;
		}

		return this.sources[count - 1].src;
	}
}


export interface ImageOptions extends ViewOptions
{
	disableVisibilityCheck?:boolean;

	srcSet?:string|IImageSource[]|ImageSourceSet;
}


const enum LoadState {
	Idle,
	Loading,
	Loaded
}


export class Image extends View implements IVisibilityTarget
{
	@Option({type:'Int', attribute:'width'})
	nativeWidth:number;

	@Option({type:'Int', attribute:'height'})
	nativeHeight:number;

	@Option({dataClass:ImageSourceSet})
	private _srcSet:ImageSourceSet;

	private _loadState:LoadState = LoadState.Idle;

	private _isInViewport:boolean = false;


	constructor(options?:ImageOptions) {
		super(options = _.defaults(options || {}, {
			tagName: 'img'
		}));

		if (this.el.hasAttribute('src') && !/^data:/.test(this.el.getAttribute('src'))) {
			if (this.el.complete && typeof this.el.naturalWidth != 'undefined' && this.el.naturalWidth != 0) {
				this.onLoad();
			} else {
				this.delegateLoad();
			}
		}

		if (!options.disableVisibilityCheck) {
			Visibility.getInstance().register(this);
		}
	}


	protected delegateLoad() {
		if (this._loadState != LoadState.Idle) return;
		this._loadState = LoadState.Loading;

		this.onLoad = _.bind(this.onLoad, this);
		this.delegate('load', null, this.onLoad);
	}


	update() {
		if (this._isInViewport) {
			this.delegateLoad();

			if (this._srcSet) {
				this.el.src = this._srcSet.get(this.el);
			}
		}
	}


	load():JQueryPromise<any> {
		var deferred = $.Deferred();

		if (this._loadState == LoadState.Loaded) {
			deferred.resolve();
		} else {
			this._isInViewport = true;
			this.update();
			this.listenToOnce(this, 'load', () => deferred.resolve());
		}

		return deferred.promise();
	}


	setSize(width:number, height:number, left?:number, top?:number) {
		var style = this.el.style;
		style.width  = width  + 'px';
		style.height = height + 'px';

		if (left != void 0) style.left = left + 'px';
		if (top  != void 0) style.top  = top  + 'px';

		this.update();
	}


	setInViewport(value:boolean) {
		if (this._isInViewport == value) return;
		this._isInViewport = value;

		this.update();
	}


	onLoad() {
		if (this._loadState == LoadState.Loaded) return;
		this._loadState = LoadState.Loaded;

		this.$el.addClass('loaded');
		this.undelegate('load', null, this.onLoad);
		this.trigger('load');
	}
}
