import {ISize} from '../../geom/size';
import {requireYouTube} from '../../utils/youtube';
import {LayerPage} from '../layer';
import {ViewOptions} from '../view';
import {LayerAdapter} from './adapter';


declare module YT {
	class Player {
		constructor(iframe:HTMLElement);
		constructor(id:string, options?:any);
		playVideo();
		pauseVideo();
		stopVideo();
	}
}


export class LayerYouTubePage extends LayerPage
{
	private _videoID:string;

	private _ratio:number = 0.5625;

	private _isLoaded:boolean;

	private _$iframe:JQuery;

	private _player:YT.Player;


	constructor(videoID:string, options?:ViewOptions) {
		super(options);

		this._videoID = videoID;
	}


	update(width:number, height:number) {
		this._$iframe.css({width, height});
	}


	measure(width:number, height:number):ISize {
		if (height / width > this._ratio) {
			return {
				width: width,
				height: width * this._ratio
			}
		} else {
			return {
				width: height / this._ratio,
				height: height
			}
		}
	}


	setCurrent(value:boolean) {
		if (value) {
			if (!this._isLoaded) {
				this._isLoaded = true;

				var $iframe = this._$iframe = $('<iframe />')
					.attr('src', 'http://www.youtube.com/embed/' + this._videoID + '?autoplay=1&enablejsapi=1')
					.attr('frameborder', 'no')
					.attr('allowfullscreen', 'yes')
					.appendTo(this.el);

				requireYouTube(() => {
					this._player = new YT.Player($iframe.get(0));
				});
			} else {
				if (this._player) {
					this._player.playVideo();
				}
			}
		} else {
			if (this._player) {
				this._player.pauseVideo();
			}
		}
	}
}


LayerAdapter.getInstance().addFactory((el:HTMLElement) => {
	var url = el.getAttribute('href');
	if (url.indexOf('?') == -1) return null;

	var chunks = url.substr(url.indexOf('?') + 1).split('&');
	for (var chunk of chunks) {
		var args = chunk.split('=', 2);
		if (args.length == 2 && args[0] == 'v') {
			return new LayerYouTubePage(args[1]);
		}
	}

	return null;
});
