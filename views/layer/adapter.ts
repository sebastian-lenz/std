import {ClickAdapter, preventDefault} from "../../handlers/click";
import {Service, Services} from "../../service";
import {View} from "../view";
import {Layer, LayerPage} from '../layer';


export interface ILayerFactory
{
	(el:Element):LayerPage;
}


@Service('LayerAdapter')
export class LayerAdapter extends View
{
	protected _layer:Layer;

	protected _scrollbar:number;

	protected _factories:ILayerFactory[] = [];


	constructor() {
		super({el: $('body')});
		new ClickAdapter(this, 'a.layer', this.onLayerLink);
	}


	addFactory(factory:ILayerFactory) {
		this._factories.push(factory);
	}


	createLayer(children:LayerPage[], index:number = 0):Layer {
		if (this._layer) return null;

		var width = this.el.offsetWidth;

		this.$el.addClass('has-layer');
		this._layer = new Layer({children, index});
		this._layer.once('remove', () => this.releaseLayer());

		width = this._scrollbar = this.el.offsetWidth - width;
		if (width > 0) {
			this.el.style.paddingRight = width + 'px';
		}

		return this._layer;
	}


	releaseLayer() {
		this._layer = null;
		this.$el.removeClass('has-layer');

		if (this._scrollbar > 0) {
			this.el.style.paddingRight = null;
		}
	}


	onLayerLink(event:Event) {
		var $target = $(event.target).parents().addBack().filter('.layer');
		$target.blur();

		var group = $target.attr('data-layer-group');
		var index = 0;

		if (group) {
			var $originalTarget = $target;
			$target = $('[data-layer-group=' + group + ']');
			index = $target.index($originalTarget);
		}

		var pages = [];
		$target.each((n, el:HTMLAnchorElement) => {
			for (var factory of this._factories) {
				var page = factory(el);
				if (page) {
					pages.push(page);
					break;
				}
			}
		});

		if (pages.length > 0) {
			this.createLayer(pages, index);
		}
	}


	static setup() {
		LayerAdapter.getInstance();
	}


	static getInstance():LayerAdapter {
		return Services.get('LayerLinkAdapter', LayerAdapter);
	}
}
