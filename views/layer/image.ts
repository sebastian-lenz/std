import {ISize} from '../../geom/size';
import {ViewOptions} from '../view';
import {LayerPage} from '../layer';
import {ResponsiveImage, ImageScaleMode} from "../responsive-image";
import {LayerAdapter} from './adapter';


export class LayerImagePage extends LayerPage
{
	private _image:ResponsiveImage;


	constructor($image:JQuery, options?:ViewOptions) {
		super(options);

		$image = $image.clone(true);
		$image
			.appendTo(this.el)
			.removeClass('loaded')
			.find('img')
			.removeClass('loaded')
			.attr('src', '')
			.removeAttr('style');

		this._image = new ResponsiveImage({
			el: $image.get(0),
			disableVisibilityCheck: true,
			disableAutoResize: true,
			scaleMode: ImageScaleMode.Fit
		});
	}


	remove():LayerImagePage {
		this._image.remove();
		super.remove();
		return this;
	}


	load():JQueryPromise<any> {
		return this._image.load();
	}


	update(width:number, height:number) {
		this._image.update(width, height);
	}


	measure(width:number, height:number):ISize {
		return this._image.measure(width, height, ImageScaleMode.Fit, 1);
	}
}


LayerAdapter.getInstance().addFactory((el:HTMLElement) => {
	if (el.hasAttribute('data-layer-image')) {
		var $image = $(el).find(el.getAttribute('data-layer-image'));
		if ($image.length) {
			return new LayerImagePage($image);
		}
	}

	return null;
});
