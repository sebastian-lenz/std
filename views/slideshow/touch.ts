import {Slideshow, SlideshowElement} from '../slideshow';
import {PointerType} from '../../handlers/delegate';
import {DragAdapter, DragDirection} from '../../handlers/drag';
import {transformName, hasTransform3D} from '../../utils/vendors';
import {Timeline} from '../../fx/timeline';
import {easeOutExpo} from '../../fx/easing/expo';


export class SlideshowDragAdapter extends DragAdapter
{
	private _slideshow:Slideshow<SlideshowElement>;

	private _slides:HTMLElement;

	private _animation:IAnimation;

	private _position:number = 0;

	private _startPosition:number = 0;

	private _shift:number = 0;

	private _shiftElement:SlideshowElement;


	constructor(slideshow:Slideshow<SlideshowElement>) {
		super({
			view: slideshow,
			selector: '.slides',
			direction: DragDirection.Horizontal
		});

		this._slideshow = slideshow;
		this._slides = <HTMLElement>slideshow.el.querySelector('.slides');
	}


	setPosition(value:number) {
		this._position = value;

		if (hasTransform3D) {
			this._slides.style[transformName] = 'translate3d(' + value + 'px,0,0)';
		} else {
			this._slides.style[transformName] = 'translate(' + value + 'px,0)';
		}
	}


	setShift(value:number) {
		if (this._shift == value) return;
		this._shift = value;

		if (this._shiftElement) {
			this._shiftElement.$el.removeClass('next previous');
		}

		var className;
		if (value > 0) {
			this._shiftElement = this._slideshow.getNext();
			className = 'next';
		} else if (value < 0) {
			this._shiftElement = this._slideshow.getPrevious();
			className = 'previous';
		} else {
			this._shiftElement = null;
		}

		if (this._shiftElement) {
			this._shiftElement.load();
			this._shiftElement.$el.addClass(className);
		}
	}


	animateTo(value:number, updateElement?:boolean) {
		this._animation = new Timeline({
			duration: 750,
			from: this._position,
			to: value,
			easing: easeOutExpo,
			update: (value) => this.setPosition(value),
			finished: () => {
				this._slideshow.unlock();
				this._animation = null;

				if (updateElement && this._shiftElement) {
					this._slideshow.setCurrent(this._shiftElement, {withoutTransition:true});
				}

				this.setPosition(0);
				this.setShift(0);
			}
		});
	}


	interruptAnimation():boolean {
		if (!this._animation) {
			return true;
		}

		var length = this._slideshow.getLength();
		if (length < 3) {
			return false;
		}

		var position = this._position;
		var from = this._slideshow.getCurrentIndex();
		this._animation.stop(true);

		var to = this._slideshow.getCurrentIndex();
		if (to == from) return;

		var diff = from - to;
		while (diff > length *  0.5) diff -= length;
		while (diff < length * -0.5) diff += length;

		var offset = this._slideshow.el.offsetWidth * (diff / Math.abs(diff));
		this.setPosition(position - offset);
		return true;
	}


	handleDragStart(x:number, y:number, deltaX:number, deltaY:number):boolean {
		if ((!this.interruptAnimation()) ||
			(this._slideshow.getLength() < 2) ||
			(this.type != PointerType.Touch) ||
			(this._slideshow.inTransition())) {
				return false;
		} else {
			this._slideshow.lock();
			this._startPosition = this._position;
			return true;
		}
	}


	handleDragMove(x:number, y:number, deltaX:number, deltaY:number):boolean {
		var position = this._startPosition + deltaX;
		var shift = position > 0 ? -1 : position < 0 ? 1 : 0;
		if (this._shift != shift) {
			this.setShift(shift);
		}

		if (!this._shiftElement) {
			position = this._startPosition + deltaX * 0.5;
		}

		this.setPosition(position);
		return true;
	}


	handleDragEnd(x:number, y:number, deltaX:number, deltaY:number) {
		var width = this._slideshow.el.offsetWidth;
		var relative = this._position / width;
		var velocity = this.getVelocity().x;

		if (Math.abs(relative) > 0.333 || Math.abs(velocity) > 20) {
			this.animateTo(-width * this._shift, true);
		} else {
			this.animateTo(0);
		}
	}


	handleDragCancel() {
		this.animateTo(0);
	}


	handleClick(x:number, y:number) { }
}
