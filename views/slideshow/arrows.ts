import {ClickAdapter} from '../../handlers/click';
import {View, ViewOptions} from '../view';
import {CycleableView} from '../cycleable';


export interface ArrowsOptions extends ViewOptions
{
	view:CycleableView<View,any>;
}


export class Arrows extends View
{
	private _view:CycleableView<View,any>;



	constructor(options?:ArrowsOptions) {
		super(options = _.defaults(options || {}, {
			tagName:   'ul',
			className: 'arrows'
		}));

		this.$el.html('<li class="previous"><a class="button">&lt;</a></li><li class="next"><a class="button">&gt;</a></li>');
		this.setView(options.view);

		new ClickAdapter(this, null, this.onButtonClick);
	}


	setView(view:CycleableView<View,any>) {
		if (this._view == view) return;

		if (this._view) {
			this._view.el.removeChild(this.el);
			this.stopListening(this._view);
		}

		this._view = view;
		if (this._view) {
			this._view.el.appendChild(this.el);
			this.setLength(this._view.getLength());
			this.onIndexChanged(this._view.getCurrentIndex());
			this.listenTo(this._view, 'indexChanged', this.onIndexChanged);
		}
	}


	setLength(value:number) {
		this.$el.toggleClass('disabled', value < 2);
	}


	onIndexChanged(index:number) {
		if (this._view.isLooped()) {
			this.$('.previous').removeClass('disabled');
			this.$('.next').removeClass('disabled');
		} else {
			this.$('.previous').toggleClass('disabled', index <= 0);
			this.$('.next').toggleClass('disabled', index >= this._view.getLength() - 1);
		}
	}


	onButtonClick(event:MouseEvent) {
		var index = this._view.getCurrentIndex();

		if ($(event.target).parents().addBack().is('.previous')) {
			index -= 1;
		} else {
			index += 1;
		}

		this._view.setCurrentIndex(index);
	}
}
