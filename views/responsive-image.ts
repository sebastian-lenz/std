import {Component} from '../component';
import {ISize} from '../geom/size';
import {Viewport} from '../services/viewport';
import {Visibility, IVisibilityTarget} from '../services/visibility';
import {View, ViewOptions, Option} from './view';
import {Image} from './image';


export enum ImageScaleMode
{
	Cover,
	Fit,
	Width,
	Height
}


export interface ResponsiveImageOptions extends ViewOptions
{
	disableVisibilityCheck?:boolean;

	disableAutoResize?:boolean;

	scaleMode?:ImageScaleMode;

	maxScale?:number;
}


@Component('.responsive-image')
export class ResponsiveImage extends View implements IVisibilityTarget
{
	@Option({type:'Number', defaultValue: 0})
	private _focusX:number;

	@Option({type:'Number', defaultValue: 0})
	private _focusY:number;

	@Option({defaultValue: ImageScaleMode.Cover})
	private _scaleMode:ImageScaleMode;

	@Option({defaultValue: Number.MAX_VALUE})
	private _maxScale:number;

	private _inViewport:boolean;

	private _image:Image;



	constructor(options?:ResponsiveImageOptions) {
		super(options || (options = {}));

		this._image = new Image({
			el: this.el.querySelector('img'),
			disableVisibilityCheck: true
		});

		if (!options.disableVisibilityCheck) {
			Visibility.getInstance().register(this);
		}

		if (!options.disableAutoResize) {
			this.listenTo(Viewport.getInstance(), 'resize', this.onWindowResize);
		}

		this.listenToOnce(this._image, 'load', this.onImageLoad);
	}


	remove():ResponsiveImage {
		this._image.remove();
		super.remove();
		return this;
	}


	load():JQueryPromise<any> {
		this.update();
		return this._image.load();
	}


	update(width?:number, height?:number) {
		width  = width  || this.el.offsetWidth;
		height = height || this.el.offsetHeight;
		var scaled = this.measure(width, height);

		var offsetX;
		var offsetY;
		switch (this._scaleMode) {
			case ImageScaleMode.Cover:
				offsetX = ResponsiveImage.calcShift(width,  scaled.width,  this._focusX);
				offsetY = ResponsiveImage.calcShift(height, scaled.height, this._focusY, true);
				break;
			case ImageScaleMode.Fit:
				offsetX = (width  - scaled.width)  * 0.5;
				offsetY = (height - scaled.height) * 0.5;
				break;
		}

		this._image.setSize(scaled.width, scaled.height, offsetX, offsetY);
	}


	measure(width:number, height:number, scaleMode:ImageScaleMode = this._scaleMode, maxScale:number = this._maxScale):ISize {
		var nativeWidth  = this._image.nativeWidth;
		var nativeHeight = this._image.nativeHeight;
		var scale        = 1;

		switch (scaleMode) {
			case ImageScaleMode.Cover:
				scale = Math.max(width / nativeWidth, height / nativeHeight);
				break;
			case ImageScaleMode.Fit:
				scale = Math.min(width / nativeWidth, height / nativeHeight);
				break;
			case ImageScaleMode.Width:
				scale = width / nativeWidth;
				break;
			case ImageScaleMode.Height:
				scale = height / nativeHeight;
				break;
		}

		scale = Math.min(scale, maxScale);

		return {
			width:  nativeWidth  * scale,
			height: nativeHeight * scale
		};
	}


	setInViewport(value:boolean) {
		if (this._inViewport == value) return;
		this._inViewport = value;
		this.update();

		this._image.setInViewport(value);
	}


	onImageLoad() {
		this.$el.addClass('loaded');
		this.trigger('load');
	}


	onWindowResize() {
		this.update();
	}


	static calcShift(container:number, image:number, focus:number, invert?:boolean):number {
		focus = (focus + 1) * 0.5;
		if (invert) focus = 1 - focus;

		var shift = Math.round(-image * focus + container / 2);
		if (shift > 0) shift = 0;
		if (shift < container - image) shift = container - image;

		return shift;
	}
}
