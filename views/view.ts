import "../polyfills/object";
import {ltrim, dasherize} from '../utils/strings';


type Initializers = {[name:string]:IInitializer};


export interface IInitializer
{
	invoke(view:View, options:ViewOptions);
}


export function getInitializer<T extends IInitializer>(owner:any, name:string, factory:{():T;}):T {
	var initializers = getInitializers(owner);

	if (initializers.hasOwnProperty(name)) {
		return <T>(initializers[name]);
	} else if (initializers[name]) {
		return <T>(initializers[name] = Object.create(initializers[name]));
	} else {
		return initializers[name] = factory();
	}
}


function getInitializers(owner:any):Initializers {
	if (owner.hasOwnProperty('__initializers')) {
		return owner.__initializers;
	} else if (owner.__initializers) {
		return owner.__initializers = Object.create(owner.__initializers);
	} else {
		return owner.__initializers = <Initializers>{};
	}
}


export interface IOptionOptions
{
	type?:string|OptionType;

	dataClass?:{new (value:any)};

	attribute?:string;

	defaultValue?:any;
}


export enum OptionType
{
	String,
	Number,
	Int,
	Bool,
	Class,
	DOM
}


export function Option(name:string, options?:IOptionOptions);
export function Option(options?:IOptionOptions);
export function Option(...args:any[])
{
	if (args.length > 0 && typeof(args[0]) === 'string') {
		let optionName = <string>args.shift();
		let options = args.length ? args.shift() : void 0;
		return function(owner:any) {
			var initializer = getInitializer<OptionInitializer>(owner.prototype, 'Option::' + optionName, () => new OptionInitializer(optionName));
			if (options) initializer.setOptions(options);
		};
	} else {
		let options = args.length ? args.shift() : void 0;
		return function(owner:any, property:string) {
			var initializer = getInitializer<OptionInitializer>(owner, 'Option::' + property, () => new OptionInitializer(property));
			initializer.setProperty(property);
			if (options) initializer.setOptions(options);
		};
	}
}


class OptionInitializer implements IInitializer
{
	private _propertyName:string;

	private _optionName:string;

	private _attributeName:string;

	private _defaultValue:any;

	private _type:OptionType;

	private _dataClass:{new (value:any)};


	constructor(optionName?:string) {
		this._optionName = optionName;
	}


	invoke(scope:View, options:ViewOptions) {
		var value;

		if (options.el && this._attributeName && options.el.hasAttribute(this._attributeName)) {
			value = options.el.getAttribute(this._attributeName);
		} else if (this._optionName && this._optionName in options) {
			value = options[this._optionName];
		} else if (this._defaultValue != void 0) {
			value = this._defaultValue;
		}

		if (value != void 0) {
			switch (this._type) {
				case OptionType.Number:
					value = parseFloat(value);
					break;
				case OptionType.Int:
					value = parseInt(value);
					break;
				case OptionType.String:
					value = '' + value;
					break;
				case OptionType.Bool:
					value = !!value;
					break;
				case OptionType.Class:
					value = value instanceof this._dataClass ? value : new this._dataClass(value);
					break;
				case OptionType.DOM:
					value = options.el ? options.el.querySelector(value) : null;
					break;
			}

			if (this._propertyName) {
				scope[this._propertyName] = value;
			} else if (this._optionName) {
				options[this._optionName] = value;
			}
		}
	}


	setProperty(value:string) {
		this._propertyName  = value;
		this._optionName    = ltrim(value, '_');
		this._attributeName = 'data-' + dasherize(this._optionName);
	}


	setOptions(options:IOptionOptions) {
		if ('defaultValue' in options) {
			this._defaultValue = options.defaultValue;
		}

		if ('type' in options) {
			if (typeof(options.type) == 'string') {
				this._type = OptionType[options.type];
			} else {
				this._type = <OptionType>options.type;
			}
		}

		if ('dataClass' in options) {
			this._dataClass = options.dataClass;
			this._type = OptionType.Class;
		}

		if ('attribute' in options) {
			this._attributeName = options.attribute;
		}
	}
}


export interface ViewOptions extends Backbone.ViewOptions<Backbone.Model>
{

}


export class View extends Backbone.View<Backbone.Model>
{
	private __initializers:Initializers;


	constructor(options?:ViewOptions) {
		super(options || (options == {}));

		for (var key in this.__initializers) {
			this.__initializers[key].invoke(this, options);
		}
	}
}
