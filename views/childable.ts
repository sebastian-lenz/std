import {View, ViewOptions, Option} from './view';


export interface ChildableViewOptions<T extends View> extends ViewOptions
{
	childSelector?:string;

	childClass?:typeof View;

	children?:T[];
}


export class ChildableView<T extends View> extends View
{
	@Option({type:'DOM'})
	protected _container:HTMLElement;

	protected _children:T[];


	constructor(options?:ChildableViewOptions<T>) {
		super(options || (options = {}));

		this._container = this._container || this.createContainer();
		this._children = [];

		if (options.childSelector && options.childClass) {
			var childClass = options.childClass;
			var elements = this.el.querySelectorAll(options.childSelector);

			_(elements).each((element) => {
				this._children.push(<T>(new childClass({el: element})));
			});
		}

		if (options.children) {
			for (var child of options.children) {
				this._children.push(child);
				this._container.appendChild(child.el);
			}
		}
	}


	remove():ChildableView<T> {
		for (var child of this._children) {
			child.remove();
		}

		super.remove();
		return this;
	}


	protected createContainer():HTMLElement {
		return this.el;
	}


	getLength():number {
		return this._children.length;
	}


	getChild(index:number):T {
		return this._children[index];
	}


	indexOf(child:T):number {
		return _(this._children).indexOf(child);
	}
}
