import {ClickAdapter} from "../handlers/click";
import {Viewport} from "../services/viewport";
import {ISize} from '../geom/size';
import {Region} from '../geom/region';
import {Timeline} from "../fx/timeline";
import {View, ViewOptions} from "./view";
import {Slideshow, SlideshowOptions, ISlideshowElement} from "./slideshow";
import {Arrows} from "./slideshow/arrows";
import {easeInOutQuad} from '../fx/easing/quad';


export class LayerPage extends View implements ISlideshowElement
{
	description:string;


	constructor(options?:ViewOptions) {
		super(options = _.defaults(options || {}, {
			tagName:   'li',
			className: 'slide'
		}));
	}


	load():JQueryPromise<any> {
		return $.Deferred().resolve().promise();
	}


	update(width:number, height:number) {

	}


	measure(width:number, height:number):ISize {
		return {width:0, height:0};
	}


	setCurrent(value:boolean):void { }
}


export interface LayerOptions extends SlideshowOptions<LayerPage>
{
	pages?:LayerPage[];
}


export class Layer extends Slideshow<LayerPage>
{
	private _region:Region;

	private _isHidden:boolean;


	constructor(options?:LayerOptions) {
		super(options = _.defaults(options || {}, {
			className:  'layer-root slideshow',
			childClass: LayerPage
		}));

		this.listenTo(Viewport.getInstance(), 'resize', this.onViewportResize);
		this.onViewportResize();

		new Arrows({view:this});

		new ClickAdapter(this, null, this.onLayerClick);
	}


	remove():Layer {
		this.trigger('remove');
		super.remove();
		return this;
	}


	_ensureElement() {
		super._ensureElement();
		this.$el.appendTo($('body'));
	}


	hide() {
		if (this._isHidden) return;
		this._isHidden = true;

		this.$el.addClass('deprecated');
		setTimeout(() => this.remove(), 250);
	}


	calculateRegion(page:LayerPage):Region {
		var width  = this.el.offsetWidth;
		var height = this.el.offsetHeight;
		var region = new Region(page.measure(width - 250, height - 250));

		region.center(width, height);
		region.x -= 20;
		region.y -= 20;

		return region;
	}


	protected createTransition(from:LayerPage, to:LayerPage):JQueryPromise<any> {
		var deferred = $.Deferred();

		this.$el
			.addClass('transition');
		from.$el
			.removeClass('current')
			.addClass('transition-from');
		to.$el
			.addClass('transition-to')
			.addClass('current');

		var fromRegion = this._region;
		var toRegion = this.calculateRegion(to);

		new Timeline({
			delay: 150,
			duration: 450,
			easing: easeInOutQuad,
			update: (value:number) => {
				var region = this._region = Region.lerp(fromRegion, toRegion, value);

				region.apply(this._container.style);

				from.update(region.width, region.height);
				to.update(region.width, region.height);
			},
			finished: () => {
				this.$el.removeClass('transition');
				from.$el.removeClass('transition-from');
				to.$el.removeClass('transition-to');

				deferred.resolve();
			}
		});

		return deferred.promise();
	}


	onLayerClick(event:Event) {
		if (event.target === this.el) {
			this.hide();
		}
	}


	onViewportResize() {
		var page = this.getCurrent();
		var region = this._region = this.calculateRegion(page);

		region.apply(this._container.style);
		page.update(region.width, region.height);
	}
}
