import {View, ViewOptions, Option} from './view'
import {CycleableView, CycleableViewOptions} from './cycleable'
import {Visibility, IVisibilityTarget} from '../services/visibility';
import {ResponsiveImage} from './responsive-image'
import {animationEnd} from '../utils/vendors'


export interface SlideshowTransitionOptions
{
	withoutTransition?:boolean;
}


export interface ISlideshowElement extends View
{
	load():JQueryPromise<any>;

	setCurrent(value:boolean):void;
}


export class SlideshowElement extends View implements ISlideshowElement
{
	private _image:ResponsiveImage;


	initialize() {
		var image = this.el.querySelector('.responsive-image');
		if (image) {
			this._image = new ResponsiveImage({
				el: image,
				disableVisibilityCheck: true
			});
		}
	}


	remove():SlideshowElement {
		this._image.remove();
		super.remove();
		return this;
	}


	load():JQueryPromise<any> {
		return this._image.load();
	}


	setCurrent(value:boolean):void { }
}


export interface SlideshowOptions<T extends ISlideshowElement> extends CycleableViewOptions<T>
{

}


export class Slideshow<T extends ISlideshowElement> extends CycleableView<T, SlideshowTransitionOptions> implements IVisibilityTarget
{
	private _transition:JQueryDeferred<any>;

	private _nextTransition:JQueryDeferred<any>;

	private _inViewport:boolean;

	private _inTransition:boolean;

	private _isLocked:boolean;


	constructor(options?:SlideshowOptions<T>) {
		super(options = _.defaults(options || {}, {
			container:     '.slides',
			childSelector: 'ul.slides > li',
			childClass:    SlideshowElement,
			isLooped:      true
		}));

		Visibility.getInstance().register(this);
	}


	setInViewport(value:boolean) {
		if (this._inViewport == value) return;
		this._inViewport = value;

		if (value) {
			var element = this.getCurrent();
			if (element) element.load();
		}
	}


	setCurrentIndex(value:number, options?:SlideshowTransitionOptions):CycleableView<T,any> {
		if (!this._isLocked) {
			super.setCurrentIndex(value, options);
		}

		return this;
	}


	inTransition():boolean {
		return this._inTransition;
	}


	lock() {
		this._isLocked = true;
	}


	unlock() {
		this._isLocked = false;
	}


	protected createContainer():HTMLElement {
		return $('<ul class="slides" />').appendTo(this.el).get(0);
	}


	protected transition(newIndex:number, oldIndex:number, options?:SlideshowTransitionOptions) {
		var from = this.getChild(oldIndex);
		var to = this.getChild(newIndex);

		if (!from || !to || !this._inViewport || (options && options.withoutTransition)) {
			if (from) {
				from.$el.removeClass('current');
				to.setCurrent(false);
			}

			if (to) {
				to.$el.addClass('current');
				to.setCurrent(true);
			}
			return;
		}

		if (this._transition) {
			this._nextTransition = this.createSequence(from, to);
		} else {
			this._transition = this.createSequence(from, to);
			this._transition.resolve();
		}
	}


	protected createSequence(from:T, to:T):JQueryDeferred<any> {
		var defered = $.Deferred();
		var promise = defered.promise();

		promise.then(() => {
			this._inTransition = true;
			return to.load();
		}).then(() => {
			return this.createTransition(from, to);
		}).then(() => {
			if (this._nextTransition) {
				this._transition = this._nextTransition;
				this._nextTransition = null;
				this._transition.resolve();
			} else {
				this._transition = null;
				this._inTransition = false;

				if (this._inViewport) {
					var element;
					if (element = this.getNext()) element.load();
					if (element = this.getPrevious()) element.load();
				}
			}
		});

		return defered;
	}


	protected createTransition(from:T, to:T):JQueryPromise<any> {
		var deferred = $.Deferred();
		from.setCurrent(false);

		this.$el
			.addClass('transition');
		from.$el
			.removeClass('current')
			.addClass('transition-from');
		to.$el
			.addClass('transition-to')
			.addClass('current')
			.on(animationEnd, () => {
				to.off(animationEnd);

				this.$el.removeClass('transition');
				from.$el.removeClass('transition-from');
				to.$el.removeClass('transition-to');
				to.setCurrent(false);

				deferred.resolve();
			});

		return deferred.promise();
	}
}
