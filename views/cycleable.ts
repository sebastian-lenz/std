import {ChildableView, ChildableViewOptions} from './childable';
import {View, ViewOptions, Option} from './view';
import {ClickAdapter} from '../handlers/click';
import {Events} from '../events';


export interface CycleableViewOptions<T extends View> extends ChildableViewOptions<T>
{
	index?:number;

	isLooped?:boolean;
}


export class CycleableView<T extends View, U> extends ChildableView<T>
{
	protected _index:number = -1;

	@Option({type:'Bool'})
	protected _isLooped:boolean;


	constructor(options?:CycleableViewOptions<T>) {
		super(options || (options = {}));

		this.setCurrentIndex(options.index != void 0 ? options.index : this.getLength() ? 0 : -1);
	}


	public normalize(index:number):number {
		var count = this.getLength();
		if (count < 1) {
			return -1;
		}

		if (this._isLooped) {
			while (index < 0) index += count;
			while (index >= count) index -= count;
		} else {
			if (index < 0) index = -1;
			if (index >= count) index = -1;
		}

		return index;
	}


	protected transition(newIndex:number, oldIndex:number, options?:U) { }


	public isLooped():boolean {
		return this._isLooped;
	}


	public getCurrentIndex():number {
		return this._index;
	}


	public setCurrentIndex(value:number, options?:U):CycleableView<T,U> {
		value = this.normalize(value);
		if (this._index === value) {
			return this;
		}

		var oldIndex = this._index;
		this._index = value;

		this.transition(value, oldIndex, options);
		this.trigger('indexChanged', value, oldIndex);
		return this;
	}


	public getCurrent():T {
		return this._children[this.getCurrentIndex()];
	}


	public setCurrent(value:T, options?:U):CycleableView<T,U> {
		return this.setCurrentIndex(this.indexOf(value), options);
	}


	public getNext():T {
		return this._children[this.normalize(this._index + 1)];
	}


	public getPrevious():T {
		return this._children[this.normalize(this._index - 1)];
	}
}


export interface CycleableIndexViewOptions extends ViewOptions
{
	view?:CycleableView<View,any>;
}


export class CycleableIndexView extends View
{
	private _view:CycleableView<View,any>;

	private _length:number = 0;

	private _index:number = -1;

	private _items:HTMLElement[] = [];



	constructor(options?:CycleableIndexViewOptions) {
		super(options = _.defaults(options || {}, {
			tagName:   'ul',
			className: 'index'
		}));

		new ClickAdapter(this, 'li', this.onItemClick);
		this.setView(options.view);
	}


	setView(view:CycleableView<View,any>) {
		if (this._view == view) return;

		if (this._view) {
			this._view.el.removeChild(this.el);
			this.stopListening(this._view);
		}

		if (view) {
			view.el.appendChild(this.el);
			this.setLength(view.getLength());
			this.setIndex(view.getCurrentIndex());
			this.listenTo(view, 'indexChanged', this.onIndexChanged);
		}

		this._view = view;
	}


	setLength(value:number) {
		if (this._length == value) {
			return;
		}

		while (this._length < value) {
			let item = document.createElement('li');
			this._items.push(item);
			this._length += 1;

			item.innerText = this._length.toString();
			this.el.appendChild(item);
		}

		while (this._length > value) {
			this.el.removeChild(this._items.pop());
			this._length -= 1;

			if (this._index == this._length) {
				this._index = -1;
			}
		}

		this._length = value;
	}


	setIndex(value:number) {
		value = Math.max(-1, Math.min(this._length - 1, Math.floor(value)));
		if (this._index == value) return;

		if (this._index != -1) {
			$(this._items[this._index]).removeClass('current');
		}

		this._index = value;
		if (this._index != -1) {
			$(this._items[this._index]).addClass('current');
		}
	}


	onItemClick(event:Event) {
		this._view.setCurrentIndex(_(this._items).indexOf(<any>event.target));
	}


	onIndexChanged(index:number) {
		this.setIndex(index);
	}
}


export class AutoPlay extends Events
{
	private _view:CycleableView<View,any>;

	private _interval:number = 5000;

	private _isPlaying:boolean = false;

	private _handle:number = Number.NaN;


	constructor(options?:{view?:CycleableView<View,any>; interval?:number, autoPlay?:boolean}) {
		super();

		options = _.defaults(options || {}, {
			interval: 5000,
			autoPlay: true
		});

		this._interval = options.interval;
		if (options.view) this.setView(options.view);
		if (options.autoPlay) this.play();
	}


	private setTimeout() {
		if (!isNaN(this._handle)) return;

		this._handle = setTimeout(() => {
			this._handle = Number.NaN;
			if (!this._view) return;

			var index = this._view.getCurrentIndex() + 1;
			if (index >= this._view.getLength()) index = 0;
			this._view.setCurrentIndex(index);
		}, this._interval);
	}


	private clearTimeout() {
		if (isNaN(this._handle)) return;

		clearTimeout(this._handle);
		this._handle = Number.NaN;
	}


	play() {
		if (this._isPlaying) return;
		this._isPlaying = true;

		this.setTimeout();
	}


	stop() {
		if (!this._isPlaying) return;
		this._isPlaying = false;

		this.clearTimeout();
	}


	setView(view:CycleableView<View,any>) {
		if (this._view == view) return;

		if (this._view) {
			this.stopListening(this._view);
		}

		if (view) {
			this.listenTo(view, 'indexChanged', this.onIndexChanged);
		}

		this._view = view;
	}


	setInterval(value:number) {
		if (this._interval == value) return;
		this._interval = value;

		if (this._isPlaying) {
			this.clearTimeout();
			this.setTimeout();
		}
	}


	onIndexChanged() {
		this.clearTimeout();

		if (this._isPlaying) {
			this.setTimeout();
		}
	}
}
