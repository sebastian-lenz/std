export interface IPoint {
	x:number;
	y:number;
}


export class Point implements IPoint
{
	x:number;

	y:number;


	constructor(x:number, y:number) {
		this.x = x;
		this.y = y;
	}


	set(point:IPoint);
	set(x:number, y:number);
	set() {
		if (arguments.length == 2) {
			this.x = arguments[0];
			this.y = arguments[1];
		} else {
			this.x = arguments[0].x;
			this.y = arguments[0].y;
		}
	}


	distance(point:IPoint):number;
	distance(x:number, y:number):number;
	distance() {
		var x = this.x, y = this.y;
		if (arguments.length == 2) {
			x -= arguments[0];
			y -= arguments[1];
		} else {
			x -= arguments[0].x;
			y -= arguments[0].y;
		}

		return Math.sqrt(x * x + y * y);
	}
}
