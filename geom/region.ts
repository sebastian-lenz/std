import {IPoint} from './point';
import {ISize, Size} from './size';

export interface IRegion extends ISize
{
	x:number;

	y:number;
}


export class Region extends Size implements IRegion
{
	x:number = 0;

	y:number = 0;


	constructor(region?:IRegion);
	constructor(size?:ISize, position?:IPoint);
	constructor(position?:IPoint, size?:ISize);
	constructor(x:number, y:number, width:number, height:number);
	constructor() {
		super();
		for (var index = 0, count = arguments.length; index < count; index++) {
			var arg = arguments[index];
			if (typeof(arg) === 'number') {
				if (index === 0) this.x      = arg;
				if (index === 1) this.y      = arg;
				if (index === 2) this.width  = arg;
				if (index === 3) this.height = arg;
			} else {
				if (arg.x      !== void 0) this.x      = arg.x;
				if (arg.y      !== void 0) this.y      = arg.y;
				if (arg.width  !== void 0) this.width  = arg.width;
				if (arg.height !== void 0) this.height = arg.height;
			}
		}
	}


	setPosition(point:IPoint);
	setPosition(width:number, height:number);
	setPosition() {
		if (arguments.length == 2) {
			this.x = arguments[0];
			this.y = arguments[1];
		} else {
			this.x = arguments[0].x;
			this.y = arguments[0].y;
		}
	}


	center(width:number, height:number) {
		this.x = (width - this.width) * 0.5;
		this.y = (height - this.height) * 0.5;
	}


	apply(style:CSSStyleDeclaration) {
		style.left   = this.x + 'px';
		style.top    = this.y + 'px';
		style.width  = this.width  + 'px';
		style.height = this.height + 'px';
	}


	static lerp(a:Region, b:Region, percent:number):Region {
		return new Region(
			a.x      + (b.x      - a.x)      * percent,
			a.y      + (b.y      - a.y)      * percent,
			a.width  + (b.width  - a.width)  * percent,
			a.height + (b.height - a.height) * percent
		);
	}
}
