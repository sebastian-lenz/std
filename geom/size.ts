export interface ISize
{
	width:number;

	height:number;
}

export class Size
{
	width:number;

	height:number;


	constructor(size?:ISize);
	constructor(width:number, height:number);
	constructor() {
		if (arguments.length > 0) {
			this.setDimensions.apply(this, arguments);
		}
	}


	setDimensions(size:ISize);
	setDimensions(width:number, height:number);
	setDimensions() {
		if (arguments.length == 2) {
			this.width  = arguments[0];
			this.height = arguments[1];
		} else {
			this.width  = arguments[0].width;
			this.height = arguments[0].height;
		}
	}


	apply(style:CSSStyleDeclaration) {
		style.width  = this.width  + 'px';
		style.height = this.height + 'px';
	}
}
