import {Viewport} from "./viewport";
import {Events} from "../events";
import {Service, Services} from "../service";


export interface IVisibilityTarget
{
	el?:HTMLElement;

	getBounds?:{():{min:number;max:number;}};

	setInViewport(value:boolean);
}


class VisibilityItem
{
	target:IVisibilityTarget;

	private _isVisible:boolean = false;

	private _min:number = Number.MAX_VALUE;

	private _max:number = Number.MIN_VALUE;


	constructor(target:IVisibilityTarget) {
		this.target = target;
	}


	updateBounds(scrollTop:number, viewMin:number, viewMax:number) {
		var min = Number.MAX_VALUE;
		var max = Number.MIN_VALUE;

		if (this.target.el) {
			let bounds = this.target.el.getBoundingClientRect();
			min = bounds.top + scrollTop;
			max = min + (bounds.height || this.target.el.offsetHeight);
		} else if (this.target.getBounds) {
			let bounds = this.target.getBounds();
			min = bounds.min;
			max = bounds.max;
		}

		if (this._min != min || this._max != max) {
			this._min = min;
			this._max = max;
			this.updateVisibility(viewMin, viewMax);
		}
	}


	updateVisibility(viewMin:number, viewMax:number) {
		var isVisible = this._max > viewMin && this._min < viewMax;
		if (this._isVisible != isVisible) {
			this._isVisible = isVisible;
			this.target.setInViewport(isVisible);
		}
	}
}


@Service('Visibility')
export class Visibility extends Events
{
	private _viewport:Viewport;

	private _items:VisibilityItem[] = [];

	private _bleed:number = 500;

	private _min:number = 0;

	private _max:number = 0;


	constructor() {
		super();

		var viewport = this._viewport = Viewport.getInstance();

		this.listenTo(viewport, 'resize', this.onViewportResize);
		this.listenTo(viewport, 'scroll', this.onViewportScroll);

		this.updateBounds();
	}


	register(target:IVisibilityTarget) {
		if (_(this._items).some((item) => item.target === target)) {
			return;
		}

		var item = new VisibilityItem(target);
		item.updateBounds(this._viewport.scrollTop, this._min, this._max);

		this._items.push(item);
	}


	updateBounds() {
		var bleed = this._bleed;

		this._min = this._viewport.scrollTop - bleed;
		this._max = this._viewport.scrollTop + this._viewport.height + bleed;
	}


	onViewportResize() {
		this.updateBounds();
		var min = this._min, max = this._max, items = this._items, scrollTop = this._viewport.scrollTop;

		for (var index = 0, count = items.length; index < count; ++index) {
			items[index].updateBounds(scrollTop, min, max);
		}
	}


	onViewportScroll() {
		this.updateBounds();
		var min = this._min, max = this._max, items = this._items;

		for (var index = 0, count = items.length; index < count; ++index) {
			items[index].updateVisibility(min, max);
		}
	}


	static getInstance():Visibility {
		return Services.get('Visibility', Visibility);
	}
}
