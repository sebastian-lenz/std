import {Events} from "../events";
import {Service, Services} from "../service";


var supportPageOffset = window.pageXOffset !== undefined;
var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat");


@Service('Viewport')
export class Viewport extends Events
{
	public width:number;

	public height:number;

	public scrollLeft:number;

	public scrollTop:number;


	constructor() {
		super();

		this.onWindowResize();
		this.onWindowScroll();

		this.onWindowResize = _.debounce(_.bind(this.onWindowResize, this), 17);
		this.onWindowScroll = _.debounce(_.bind(this.onWindowScroll, this), 17);

		if (window.addEventListener) {
			window.addEventListener('resize', this.onWindowResize);
			window.addEventListener('scroll', this.onWindowScroll);
		} else {
			(<any>window).attachEvent('onresize', this.onWindowResize);
			(<any>window).attachEvent('onscroll', this.onWindowScroll);
			setTimeout(() => this.trigger('resize', this.width, this.height), 50);
		}
	}


	onWindowResize() {
		var width  = Math.max(document.documentElement.clientWidth  || 0);
		var height = Math.max(document.documentElement.clientHeight || 0);

		if (this.width != width || this.height != height) {
			this.width  = width;
			this.height = height;
			this.trigger('resize', width, height);
		}
	}


	onWindowScroll() {
		var scrollLeft = supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft : document.body.scrollLeft;
		var scrollTop  = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop  : document.body.scrollTop;

		if (this.scrollLeft != scrollLeft || this.scrollTop != scrollTop) {
			this.scrollLeft = scrollLeft;
			this.scrollTop  = scrollTop;
			this.trigger('scroll', scrollLeft, scrollTop);
		}
	}


	static getInstance():Viewport {
		return Services.get('Viewport', Viewport);
	}
}
