import {Events} from '../events';
import {Service, Services} from '../service';
import {Viewport} from './viewport';


export var getDeviceWidth:{():number};

export var getDeviceHeight:{():number};

(function() {
	var scope:any = window;
	var prefix = 'inner';
	if (!('innerWidth' in window )) {
		prefix = 'client';
		scope = document.documentElement || document.body;
	}

	getDeviceWidth = () => scope[prefix + 'Width'];
	getDeviceHeight = () => scope[prefix + 'Height'];
})();


export interface IBreakpoint
{
	name:string;

	minWidth:number;

	container:number;

	update?:{(breakpoint:IBreakpoint, width:number)}
}


@Service('Grid')
export class Grid extends Events
{
	current:number = 0;

	breakpoints:IBreakpoint[] = [{
		name:      'xs',
		minWidth:  0,
		container: 0,
		update: (breakpoint:IBreakpoint, width) => breakpoint.container = width - 30,
	},{
		name:      'sm',
		minWidth:  768,
		container: 750,
	},{
		name:      'md',
		minWidth:  992,
		container: 970
	},{
		name:      'lg',
		minWidth:  1200,
		container: 1170
	}];


	constructor() {
		super();

		var viewport = Viewport.getInstance();
		viewport.on('resize', this.onViewportResize);
		this.onViewportResize(viewport.width);
	}


	onViewportResize(width:number) {
		var deviceWidth = getDeviceWidth();

		var index = 0;
		var length = this.breakpoints.length;
		while (index < length - 1) {
			if (this.breakpoints[index + 1].minWidth > deviceWidth) {
				break;
			} else {
				index += 1;
			}
		}

		var breakpoint = this.breakpoints[index];
		if (breakpoint.update) {
			breakpoint.update(breakpoint, width);
		}

		if (this.current != index || breakpoint.update) {
			this.current = index;
			this.trigger('changed', breakpoint);
		}
	}


	static getInstance():Grid {
		return Services.get('Grid', Grid);
	}
}
