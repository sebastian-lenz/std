interface IAnimation
{
	isRunning():boolean;

	stop(finish?:boolean);
}


interface IEasingFunction
{
	(time:number, base:number, change:number, duration:number):number;
}
