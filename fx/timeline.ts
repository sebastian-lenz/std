import '../polyfills/requestAnimationFrame';


export var now = (function() {
	var performance = <any>window.performance;
	var now = performance && (performance.now || performance.webkitNow || performance.msNow || performance.mozNow);
	if (now) {
		return function () {
			return now.call(performance)
		}
	} else {
		return function () {
			return +new Date();
		}
	}
})();


export var defaultEasing:IEasingFunction = function(t, b, c, d) {
	return -c *(t/=d)*(t-2) + b;
};


export interface TimelineOptions
{
	duration:number;

	delay?:number;

	from?:number;

	to?:number;

	easing?:IEasingFunction;

	update?:{(value:number)};

	finished?:{()};
}


export class Timeline implements IAnimation
{
	private _duration:number;

	private _time:number;

	private _base:number;

	private _change:number;

	private _easing:IEasingFunction;

	private _lastFrame:number;

	private _isRunning:boolean;

	private _isFinished:boolean;


	constructor(options:TimelineOptions) {
		this._duration = options.duration;
		this._time = options.delay ? -options.delay : 0;
		this._easing = options.easing ? options.easing : defaultEasing;

		if (options.from != void 0 && options.to != void 0) {
			this._base = options.from;
			this._change = options.to - options.from;
		} else {
			this._base = 0;
			this._change = 1;
		}

		if (options.update) this.update = options.update;
		if (options.finished) this.finished = options.finished;

		this.play();
	}


	isRunning():boolean {
		return this._isRunning;
	}


	play() {
		if (this._isRunning || this._isFinished) return;
		this._isRunning = true;
		this.requestFrame();
	}


	stop(finish?:boolean) {
		if (!this._isRunning) return;
		this._isRunning = false;

		if (finish && !this._isFinished) {
			this._isFinished = true;
			this.finished();
		}
	}


	private requestFrame() {
		this._lastFrame = now();
		window.requestAnimationFrame(() => this.onFrame());
	}


	protected update(value:number) { }


	protected finished() { }


	private onFrame() {
		if (!this._isRunning) return;
		var delta = now() - this._lastFrame;
		var time = this._time += delta;

		if (time >= this._duration) {
			this._isFinished = true;
			this._isRunning = false;
			this.update(this._easing(this._duration, this._base, this._change, this._duration));
			this.finished();
		} else if (time >= 0) {
			this.update(this._easing(time, this._base, this._change, this._duration));
			this.requestFrame();
		} else {
			this.requestFrame();
		}
	}
}
