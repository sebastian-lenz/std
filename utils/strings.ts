function defaultToWhiteSpace(characters:any):string {
	if (characters == null) {
		return '\\s';
	} else if (characters.source) {
		return characters.source;
	} else {
		return '[' + escapeRegExp(characters) + ']';
	}
}


export function escapeRegExp(str:string):string {
	return str.replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
}


export function trim(str:string, characters?:string):string {
	characters = defaultToWhiteSpace(characters);
	return str.replace(new RegExp('^' + characters + '+|' + characters + '+$', 'g'), '');
}


export function ltrim(str:string, characters?:string) {
	characters = defaultToWhiteSpace(characters);
	return str.replace(new RegExp('^' + characters + '+'), '');
}


export function dasherize(str:string):string {
	return trim(str).replace(/([A-Z])/g, '-$1').replace(/[-_\s]+/g, '-').toLowerCase();
}
