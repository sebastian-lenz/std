export var transformName:string = null;

export var transformOriginName:string = null;

export var hasTransform3D:boolean = false;

export var transitionName:string = null;

export var transitionEnd:string = null;

export var animationName:string = null;

export var animationEnd:string = null;



var prefixes = ['ms', 'Webkit', 'Moz', 'O'];

var el:HTMLDivElement;

export function eachPrefix(name:string, callback:{(prop:string, prefix?:string):boolean}):string {
	if (callback(name)) {
		return name;
	}

	name = name.substr(0, 1).toUpperCase() + name.substr(1);
	for (var index = 0, count = prefixes.length; index < count; index++) {
		var prefix = prefixes[index];
		var prefixedName = prefix + name;

		if (callback(prefixedName, prefix)) {
			if (index > 0) {
				prefixes.splice(index, 1).unshift(prefix);
			}

			return prefixedName;
		}
	}

	return null;
}


export function withDummyElement<T>(callback:{(el:HTMLDivElement):T}):T {
	var isCreator:boolean;
	if (!el) {
		isCreator = true;
		el = document.createElement('div');
		document.body.insertBefore(el, null);
	}

	var result = callback(el);

	if (isCreator) {
		document.body.removeChild(el);
		el = void 0;
	}

	return result;
}


export function getPrefixedStyle(name:string, callback?:{(name:string, prefix?:string)}):string {
	return withDummyElement((el) => {
		return eachPrefix(name, (name:string, prefix?:string):boolean => {
			if (el.style[name] !== undefined) {
				if (callback) callback(name, prefix);
				return true;
			} else {
				return false;
			}
		});
	});
}


export function getPrefixedEvent(name:string, prefix?:string):string {
	if (prefix == 'Webkit') return 'webkit' + name;
	if (prefix == 'O') return 'o' + name;
	return name.toLowerCase();
}


withDummyElement((el) => {
	transformName = getPrefixedStyle('transform', (name:string, prefix?:string) => {
		var name = (prefix ? '-' + prefix.toLowerCase() + '-' : '') + 'transform';
		el.style[name] = 'translate3d(100px, 0, 0)';
		hasTransform3D = !!window.getComputedStyle(el).getPropertyValue(name);
	});

	transformOriginName = getPrefixedStyle('transformOrigin');

	animationName = getPrefixedStyle('animation', (name:string, prefix?:string) => {
		animationEnd = getPrefixedEvent('AnimationEnd', prefix);
	});

	transitionName = getPrefixedStyle('transition', (name:string, prefix?:string) => {
		transitionEnd = getPrefixedEvent('TransitionEnd', prefix);
	});
});
