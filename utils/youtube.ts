var isYouTubeReady:boolean = false;

var loader:JQueryPromise<any>;


export function requireYouTube(callback:Function)
{
	if (isYouTubeReady) {
		callback();
	} else {
		loader = createLoader().then(<any>callback);
	}
}


function createLoader():JQueryPromise<any> {
	if (loader) {
		return loader;
	}

	var deferred = $.Deferred();

	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";

	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	window['onYouTubeIframeAPIReady'] = function() {
		isYouTubeReady = true;
		deferred.resolve();
	};

	return deferred.promise();
}
