import {View} from '../views/view';


var $document = $(document);

var pointerPrefix = window.navigator.pointerEnabled ? '' : window.navigator.msPointerEnabled ? 'ms' : '';

var hasTouchEvents = 'ontouchstart' in window;

export var delegate:{(adapter:IPointerAdapter, view:View, selector?:string)};
if ('on' + pointerPrefix + 'pointerdown' in window) {
	delegate = delegatePointerEvents;
} else {
	delegate = delegateMouseEvents;
}


export interface IPointerAdapter
{
	handleDown(event:Event, x:number, y:number, type:PointerType):boolean;
	handleMove(event:Event, x:number, y:number):boolean;
	handleUp(event:Event, x:number, y:number);
	handleCancel(event:Event);
}


export const enum PointerType {
	Mouse,
	Touch
}


const enum CaptureState {
	Idle,
	Mouse,
	Touch
}


function delegatePointerEvents(adapter:IPointerAdapter, view:View, selector?:string)
{
	var isPointerDown:boolean = false;

	var pointerID:number = Number.NaN;


	function pointerDown(e:JQueryEventObject) {
		if (isPointerDown) return;
		var event = <PointerEvent>e.originalEvent;
		pointerID = event.pointerId;

		if (adapter.handleDown(event, event.pageX, event.pageY, event.pointerType == 'mouse' ? PointerType.Mouse : PointerType.Touch)) {
			isPointerDown = true;
			$document.on(pointerPrefix + 'pointermove', pointerMove);
			$document.on(pointerPrefix + 'pointerup', pointerUp);
			$document.on(pointerPrefix + 'pointercancel', pointerCancel);
		}
	}


	function pointerMove(e:JQueryEventObject) {
		var event = <PointerEvent>e.originalEvent;
		if (event.pointerId != pointerID) return;

		if (!adapter.handleMove(event, event.pageX, event.pageY)) {
			releasePointer();
			adapter.handleCancel(event);
		}
	}


	function pointerUp(e:JQueryEventObject) {
		var event = <PointerEvent>e.originalEvent;
		if (event.pointerId != pointerID) return;

		releasePointer();
		adapter.handleUp(event, event.pageX, event.pageY);
	}


	function pointerCancel(e:JQueryEventObject) {
		var event = <PointerEvent>e.originalEvent;
		if (event.pointerId != pointerID) return;

		releasePointer();
		adapter.handleCancel(event);
	}


	function releasePointer() {
		isPointerDown = false;
		pointerID = Number.NaN;
		$document.off(pointerPrefix + 'pointermove', pointerMove);
		$document.off(pointerPrefix + 'pointerup', pointerUp);
	}


	view.delegate(pointerPrefix + 'pointerdown', selector, pointerDown);
}


function delegateMouseEvents(adapter:IPointerAdapter, view:View, selector?:string)
{
	var state:CaptureState = CaptureState.Idle;

	var identifier:number = Number.NaN;

	var touchCooldown:number;


	function mouseDown(e:JQueryMouseEventObject) {
		if (state != CaptureState.Idle || touchCooldown) return;
		if (adapter.handleDown(e.originalEvent, e.pageX, e.pageY, PointerType.Mouse)) {
			state = CaptureState.Mouse;
			$document.on('mousemove', mouseMove);
			$document.on('mouseup', mouseUp);
		}
	}


	function mouseMove(e:JQueryMouseEventObject) {
		if (!adapter.handleMove(e.originalEvent, e.pageX, e.pageY)) {
			releaseMouse();
			adapter.handleCancel(e.originalEvent);
		}
	}


	function mouseUp(e:JQueryMouseEventObject) {
		releaseMouse();
		adapter.handleUp(e.originalEvent, e.pageX, e.pageY);
	}


	function releaseMouse() {
		state = CaptureState.Idle;
		$document.off('mousemove', mouseMove);
		$document.off('mouseup', mouseUp);
	}


	if (hasTouchEvents) {
		function touchStart(e:JQueryEventObject) {
			if (state != CaptureState.Idle) return;

			var event = <any>e.originalEvent;
			if (event.changedTouches.length === 0) return;
			var touch = event.changedTouches[0];
			identifier = touch.identifier;

			if (adapter.handleDown(e.originalEvent, touch.pageX, touch.pageY, PointerType.Touch)) {
				state = CaptureState.Touch;
				$document.on('touchmove', touchMove);
				$document.on('touchcancel', touchCancel);
				$document.on('touchend', touchEnd);
			}
		}


		function touchMove(e:JQueryEventObject) {
			withCurrentTouch(e, (touch:any) => {
				if (!adapter.handleMove(e.originalEvent, touch.pageX, touch.pageY)) {
					releaseTouch();
					adapter.handleCancel(e.originalEvent);
				}
			});
		}


		function touchEnd(e:JQueryEventObject) {
			withCurrentTouch(e, (touch:any) => {
				releaseTouch();
				adapter.handleUp(e.originalEvent, touch.pageX, touch.pageY);
			});
		}


		function touchCancel(e:JQueryEventObject) {
			withCurrentTouch(e, (touch:any) => {
				releaseTouch();
				adapter.handleCancel(e.originalEvent);
			});
		}


		function withCurrentTouch(e:JQueryEventObject, callback:Function) {
			var event = <any>e.originalEvent;
			for (var touch of event.changedTouches) {
				if (touch.identifier == identifier) {
					callback(touch);
					break;
				}
			}
		}


		function releaseTouch() {
			identifier = Number.NaN;
			state = CaptureState.Idle;
			$document.off('touchmove', touchMove);
			$document.off('touchcancel', touchCancel);
			$document.off('touchend', touchEnd);

			if (touchCooldown) clearTimeout(touchCooldown);
			touchCooldown = setTimeout(() => {
				touchCooldown = null;
			}, 1000);
		}


		view.delegate('touchstart', selector, touchStart);
	}

	view.delegate('mousedown', selector, mouseDown);
}
