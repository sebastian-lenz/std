import {delegate, IPointerAdapter, PointerType} from './delegate';
import {Point, IPoint} from '../geom/point';
import {View} from '../views/view';
import {preventClick, preventDefault} from './click';
export {PointerType} from './delegate';


const enum DragState
{
	Idle,
	Watching,
	Draging
}


export enum DragDirection
{
	Horizontal = 1,
	Vertical = 2,
	Both = Horizontal | Vertical
}


export interface ITrackingPoint extends IPoint
{
	time:number;
}


export interface DragAdapterOptions
{
	view:View;

	selector?:string;

	direction?:string|DragDirection;

	threshold?:number;

	trackPeriod?:number;
}


export class DragAdapter implements IPointerAdapter
{
	downX:number = 0;

	downY:number = 0;

	state:DragState = DragState.Idle;

	direction:DragDirection = DragDirection.Both;

	threshold:number = 5;

	track:ITrackingPoint[] = [];

	trackPeriod:number = 100;

	type:PointerType;


	constructor(options:DragAdapterOptions) {
		delegate(this, options.view, options.selector);

		if (options.threshold) {
			this.threshold = options.threshold;
		}

		if (options.trackPeriod) {
			this.trackPeriod = options.trackPeriod;
		}

		if (options.direction) {
			this.direction = typeof(options.direction) === 'string' ? DragDirection[options.direction] : <DragDirection>options.direction;
		}
	}


	handleDragStart(x:number, y:number, deltaX:number, deltaY:number):boolean {
		return true;
	}


	handleDragMove(x:number, y:number, deltaX:number, deltaY:number):boolean {
		return true;
	}


	handleDragEnd(x:number, y:number, deltaX:number, deltaY:number) { }


	handleDragCancel() {}


	handleClick(x:number, y:number) { }


	addTrackingPoint(x:number, y:number, time:number = +(new Date())) {
		this.reviseTrackingPoints(time);
		this.track.push({x, y, time});
	}


	reviseTrackingPoints(time:number = +(new Date())) {
		while (this.track.length > 0) {
			if (time - this.track[0].time <= this.trackPeriod) break;
			this.track.shift();
		}
	}


	getVelocity():Point {
		var count = this.track.length;
		if (count < 2) {
			return new Point(0, 0);
		}

		var first    = this.track[0];
		var last     = this.track[count - 1];
		var duration = (last.time - first.time) / 15;

		return new Point(
			(last.x - first.x) / duration,
			(last.y - first.y) / duration
		);
	}


	handleDown(event:Event, x:number, y:number, type:PointerType):boolean {
		if (this.state != DragState.Idle) {
			return false;
		}

		this.type = type;
		this.downX = x;
		this.downY = y;
		this.state = DragState.Watching;
		this.track = [];
		this.addTrackingPoint(x, y);

		if (this.type == PointerType.Mouse) {
			preventDefault(event);
		}

		return true;
	}


	handleMove(event:Event, x:number, y:number):boolean {
		this.addTrackingPoint(x, y);
		var deltaX = x - this.downX;
		var deltaY = y - this.downY;

		if (this.state === DragState.Watching) {
			if (this.type == PointerType.Mouse) {
				preventDefault(event);
			}

			var distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
			if (distance < this.threshold) {
				return true;
			}

			if (this.direction == DragDirection.Horizontal) {
				if (Math.abs(deltaX) < Math.abs(deltaY)) {
					return false;
				}
			} else if (this.direction == DragDirection.Vertical) {
				if (Math.abs(deltaX) > Math.abs(deltaY)) {
					return false;
				}
			}

			if (this.handleDragStart(x, y, deltaX, deltaY)) {
				this.state = DragState.Draging;
			} else {
				return false;
			}
		}

		if (this.state === DragState.Draging) {
			preventDefault(event);

			if (this.handleDragMove(x, y, deltaX, deltaY)) {
				return true;
			} else {
				this.state = DragState.Idle;
				this.handleDragCancel();
				return false;
			}
		}

		return false;
	}


	handleUp(event:Event, x:number, y:number) {
		this.addTrackingPoint(x, y);

		if (this.state == DragState.Draging) {
			var deltaX = x - this.downX;
			var deltaY = y - this.downY;
			this.handleDragEnd(x, y, deltaX, deltaY);
			preventClick();
		} else {
			this.handleClick(x, y);
		}

		this.state = DragState.Idle;
	}


	handleCancel() {
		if (this.state == DragState.Draging) {
			this.handleDragCancel();
		}

		this.state = DragState.Idle;
	}
}
