import {delegate, IPointerAdapter, PointerType} from './delegate';
import {getInitializer, IInitializer, View, ViewOptions} from '../views/view';

var isClickPrevented:boolean = false;

var preventClickTimeout;

$(document).on('click', (e:JQueryMouseEventObject) => {
	if (isClickPrevented) {
		e.preventDefault();
		e.stopImmediatePropagation();
		isClickPrevented = false;
	}
});


export function preventClick() {
	if (preventClickTimeout) clearTimeout(preventClickTimeout);
	isClickPrevented = true;
	preventClickTimeout = setTimeout(() => {
		isClickPrevented = false;
		preventClickTimeout = null;
	}, 500);
}


export function preventDefault(event:Event) {
	if (event && event.preventDefault) {
		event.preventDefault();
	} else if (window.event && window.event.returnValue) {
		window.event.returnValue = false;
	}
}


export class ClickAdapter implements IPointerAdapter
{
	callback:Function;

	view:View;

	startX:number;

	startY:number;


	constructor(view:View, selector:string, callback:Function) {
		delegate(this, view, selector);

		this.callback = callback;
		this.view = view;
	}


	handleDown(event:Event, x:number, y:number, type:PointerType):boolean {
		this.startX = x;
		this.startY = y;
		return true;
	}


	handleMove(event:Event, x:number, y:number):boolean {
		x -= this.startX;
		y -= this.startY;
		return Math.sqrt(x * x + y * y) < 5;
	}


	handleUp(event:Event, x:number, y:number) {
		this.callback.call(this.view, event);
		preventClick();
	}


	handleCancel() { }
}
